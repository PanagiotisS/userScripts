// ==UserScript==
// @name        Steam Achievements Sorter
// @version     1
// @grant    		none
// @description Sort steam achievements in chronological order
// @include     /https://steamcommunity.com/profiles/.*/stats/.*/?tab=achievements/
// @author      PanagiotisS
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==



function findDate(text1) {
  // Find five groups, 1: DD?, 2: MMM, 3: YYYY, 4: HH:MM 5: am/pm
  var reg1 = new RegExp('Unlocked ([0-9]{1,2}) ([a-zA-Z]{3})(, [0-9]{4} )? ?@ (1?[0-9]:[0-5][0-9])([a|p]m)');
  var date1 = $(text1).text().match(reg1);
  
  if (date1 !== null) {
    // Find the time and convert from am/pm to 24h
    var time1 = date1[4].split(':')
    time1 = date1[5] == 'pm' ? (parseInt(time1[0]) + 12).toString() + ":" + time1[1] : date1[4];
    
    // Find the year, if there is no year use current year
    var year = date1[3];
    if (year == null) {
      year = new Date().getFullYear();
      year = ', ' + year + ' ';
    }
    
    // Construct the date as: Feb 26, 2019 14:12
    date1 = date1[2] + ' ' + date1[1] + year + time1
    date1 = new Date(date1);
    
  } else {  // If date not found it (probably) means that the achievement is not unlocked
    date1 = 0;
  }    

  return date1
}

function sortDescending(a, b) {
  var date1 = findDate($(a));
  var date2 = findDate($(b));
  return date1 < date2;
}

$(document).ready(function() {
  $('#personalAchieve .achieveRow').sort(sortDescending).appendTo('#personalAchieve');
});
