// ==UserScript==
// @name            Reddit: Close login-popup
// @description     Closes reddit login popup for desktop users.
// @author          PanagiotisS
// @include         /^https?://.*\.reddit\.com/.*$/
// @grant           none
// @version         1.0
// ==/UserScript==

window.addEventListener('load', function() {
  document.getElementsByClassName("skip-for-now")[0].click()
    // document.getElementsByClassName("m20vbv-2 iiyDuW")[0].click()
    }, false);
})
