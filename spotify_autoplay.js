// ==UserScript==
// @name         Spotify autoplay
// @namespace    https://gitlab.com/panagiotiss
// @version      0.4
// @description  Pause/unpause when spotify tries to play ads
// @author       panagiotiss
// @grant        none
// @include      https://open.spotify.com/*
// @run-at document-idle
// ==/UserScript==


// The observer detects when the title changes
// then it check if the title starts with advertisement
// if it does it will press the pause button
// then it will call the press_play function
var observer1 = new MutationObserver(function(mutations) {
    // var current_title = document.title.toLocaleLowerCase();
    var current_title = mutations[0].target.text.toLocaleLowerCase();

    if (current_title.startsWith("advertisement")) {
        // console.log("Advertisement detect: " + current_title);
        document.querySelector('.player-controls .spoticon-pause-16').click();
    } else if (current_title.startsWith("spotify")) {
        // console.log("Spotify not playing: " + current_title);
        document.querySelector('.player-controls .spoticon-play-16').click();
    // } else {
        // console.log("NOT advertisement : " + current_title);
    }
});

function autoplayStart() {
    // console.log("Starting observer");
    observer1.observe(
        document.querySelector('title'), {
            childList: true
        }
    );
    document.querySelector('.player-controls .spoticon-play-16').click();
}

function autoplayStop() {
    // console.log("Disconnecting observer");
    observer1.disconnect();
    document.querySelector('.player-controls .spoticon-pause-16').click();
}

// Add buttons to control the observer

// Button 2 to stop the observer
var li2 = document.createElement('li');
li2.setAttribute("class", "navBar-item");

var div_b2 = document.createElement('div');
div_b2.setAttribute("class", "GlueDropTarget");

var button2 = document.createElement("button");
button2.type = "button";
button2.onclick = autoplayStop;
button2.setAttribute("class", "fcdf941c8ffa7d0878af0a4f04aa05bb-scss link-subtle navBar-link");

const SVG_NS = 'http://www.w3.org/2000/svg';
var svg2 = document.createElementNS(SVG_NS, "svg");
svg2.setAttribute("class", "_600e6b37610fb50fa7f2994258a51bf6-scss");
svg2.setAttribute(null, "shape-rendering", "crispEdges");
svg2.setAttributeNS(null, "viewBox", "0 0 1024 1024");
svg2.setAttributeNS(null, "width", "24");
svg2.setAttributeNS(null, "height", "24");

var path2 = document.createElementNS(SVG_NS, "path");
path2.setAttributeNS(null, "d", "M352 768h-1q-13 0-22-9.5t-9-22.5V288q0-13 9-22.5t22-9.5h1q13 0 22.5 9.5T384 288v448q0 13-9.5 22.5T352 768zm321 0h-1q-13 0-22.5-9.5T640 736V288q0-13 9.5-22.5T672 256h1q13 0 22.5 9.5T705 288v448q0 13-9.5 22.5T673 768zM512 64q91 0 174 35 81 34 143 96t96 143q35 83 35 174t-35 174q-34 81-96 143t-143 96q-83 35-174 35t-174-35q-81-34-143-96T99 686q-35-83-35-174t35-174q34-81 96-143t143-96q83-35 174-35zm0-64Q373 0 255 68.5T68.5 255 0 512t68.5 257T255 955.5t257 68.5 257-68.5T955.5 769t68.5-257-68.5-257T769 68.5 512 0z");

var span2 = document.createElement("span");
span2.setAttribute("class", "eebcc32f0688af5e05f2537e14c59633-scss f92d0df6ae4da826d7d532fbf608d469-scss");
span2.setAttribute("as", "span");
span2.innerHTML = "Autoplay stop";

// Copy button2 elements to create button 1
var li1 = li2.cloneNode(true);
var div_b1 = div_b2.cloneNode(true);
var button1 = button2.cloneNode(true);
var svg1 = svg2.cloneNode(true);
var span1 = span2.cloneNode(true);

var path1 = document.createElementNS(SVG_NS, "path");
path1.setAttributeNS(null, "d", "M512 64q91 0 174 35 81 34 143 96t96 143q35 83 35 174t-35 174q-34 81-96 143t-143 96q-83 35-174 35t-174-35q-81-34-143-96T99 686q-35-83-35-174t35-174q34-81 96-143t143-96q83-35 174-35zm0-64Q373 0 255 68.5T68.5 255 0 512t68.5 257T255 955.5t257 68.5 257-68.5T955.5 769t68.5-257-68.5-257T769 68.5 512 0zm-64 316l231 196-231 196V316zm-40-112q-9 0-16.5 6.5T384 226v572q0 9 7.5 15t17 6 16.5-6l335-285q8-7 8-16t-8-16L425 211q-7-7-17-7z");

// Make the changes for button 1
button1.onclick = autoplayStart;
span1.innerHTML = "Autoplay start";

// Group buton2 elements
li2.appendChild(div_b2);
div_b2.appendChild(button2);
button2.appendChild(svg2);
svg2.appendChild(path2);
button2.appendChild(span2);

// Group button1 elements
li1.appendChild(div_b1);
div_b1.appendChild(button1);
button1.appendChild(svg1);
svg1.appendChild(path1);
button1.appendChild(span1);

// Collect both buttons
var ul_b = document.createElement('ul');
ul_b.appendChild(li1);
ul_b.appendChild(li2);

// Add the buttons at the end of .Root__nav-bar
// Use an observer to check when .Root__nav-bar is loaded
// https://stackoverflow.com/a/47406751
(new MutationObserver(check)).observe(document, {
    childList: true,
    subtree: true
});

function check(changes, observer) {
    if (document.querySelector('.Root__nav-bar')) {
        observer.disconnect();
        document.getElementsByClassName("Root__nav-bar")[0].firstChild.appendChild(ul_b);
    }
}