// ==UserScript==
// @name     		Twitch: Stop auto-play
// @version  		1
// @grant    		none
// @description Pause the video at Twitch.tv home page
// @include     https://www.twitch.tv/
// ==/UserScript==

function pauser() {
  document.getElementsByClassName("player-button qa-pause-play-button")[0].click();
  }
  setTimeout (pauser, 2000);