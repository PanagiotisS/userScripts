// ==UserScript==
// @name            Reddit: Redirect to Old Reddit
// @description     Redirects to the old.reddit.com
// @author          PanagiotisS
// @match           https://www.reddit.com/*
// @run-at          document-start
// @grant           none
// @version         1.0
// ==/UserScript==

var currentURL = window.document.location.toString();

if (currentURL.includes("//www")) {
  var newURL = currentURL.replace("//www", "//old");
  window.document.location.replace(newURL);
}
